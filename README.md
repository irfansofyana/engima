# Perubahan Engima

## Deskripsi Singkat

Setelah aplikasi web Engima diluncurkan, bioskop Engi menjadi sangat laku. Sebelumnya, Engi mengurus semua transaksi tiket film dan penambahan data film secara manual. Karena kewalahan, Engi memutuskan untuk menggunakan web service untuk mempermudah pekerjaannya. Engi meminta Anda untuk mengimplementasikan perubahan pada website Engima beserta web service dan aplikasi Bank yang digunakan untuk transaksi tiket film di Engima.

Karena pemanfaatan web service Transaksi dan TheMovieDB, berikut perubahan pada aplikasi Engima:
- Pengambilan data film diambil dari API TheMovieDB.
- Pada halaman Home, film yang ditampilkan adalah film yang memiliki tanggal rilis tidak lebih dari 7 hari dari hari ini.
- Pada halaman Film Details, akan ada dua jenis rating. Satu rating dari ulasan pengguna, satu lagi dari critics (rating dari TheMovieDB).
- Pada halaman Buy Ticket, jika pengguna membeli tiket, akan muncul id transaksi dan nomor akun virtual unik untuk pembayaran tiket. Gunakan popup yang sebelumnya menampilkan status transaksi sukses/gagal.
- Pada halaman Transaction History, ditampilkan id dan status pembayaran setiap transaksi tiket film. Masa berlaku suatu transaksi adalah 2 menit. Status pembayaran diperbaharui jika halaman diperbaharui (refresh). Transaksi yang sudah memiliki status “Cancelled” tidak dapat dilunaskan lagi sehingga pembayaran terhadap nomor akun virtual terkait setelah masa berlaku pembayaran tidak lagi diperhitungkan.
- Transaksi tiket film pada halaman Transaction History didapat dari basis data Transaksi. Basis data Engima tidak menyimpan data transaksi pembelian tiket.

## Perubahan Basis Data
- Tabel Book dihapaus karena sekarang berada di WS-Transaksi
- Tabel Category dihapus karena diambil dari TheMovieDB
- Tabel Movie dihapus karena diambil dari TheMovieDB
- Tabel MovieCategory dihapus karena diambil dari TheMovieDB

## Screenshot Perubahan Engima
### Home
Data film yang sedang tayang diperoleh dari TheMovieDB
![](img/home.png)

### Search
Data film yang dicari diperoleh dari TheMovieDB
![](img/search.png)

### Transaction History
Data transaksi user diperoleh dari WS Transaksi
![](img/history.png)

### Movie Detail
Terdapat 2 buah rating yang ditampilkan
![](img/movie-detail.png)

### Book Schedule
Jika berhasil memesan tiket akan ditampilkan rekening virtual dan idTransksi
![](img/payment.png)

### Payment Success
Jika berhasil pemesanan berhasil dapat melalukan review
![](img/payment-success.png)

## Pembagian Kerja
- Penggunaan TheMovieDB (Home+Search): 13517075
- Film Details: 13517057
- Buy Ticket: 13517078
- Transaction History: 13517075
- Ci/CD: 13517078
- Eksplorasi setup mesin deployment: 13517078

## URL
http://ec2-54-227-190-218.compute-1.amazonaws.com/engima/public