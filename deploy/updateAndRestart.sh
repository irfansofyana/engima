#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
sudo rm -rf engima
sudo rm -rf /var/www/html/engima

# clone the repo again
git clone git@gitlab.informatika.org:if3110-2019-02-k03-01/engima.git

# enter directory
sudo mv engima/ /var/www/html/