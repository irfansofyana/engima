<?php namespace controllers;

class Api extends \core\Controller
{

    public function validate()
    {
        $data = new \stdClass();
        $data->status = 200;
        if ($this->model('RegisterModel')->validate()) {
            $data->result = true;
        } else {
            $data->result = false;
        }
        $response = json_encode($data);
        echo $response;
    }

    public function book()
    {
        $auth = new \core\Auth;
        $idUser = $auth->getUserId();

        $data = new \stdClass();
        $data->status = 200;

        $result = $this->model('BookModel')->insertSeat($_POST, $idUser);
        if ($result != -1) {
            $data->result = true;
            $data->idTransaksi = $result;
        } else {
            $data->result = false;
        }

        $response = json_encode($data);
        echo $response;
    }


    public function search()
    {
        $keyword = $_GET['q'];
        $page = 1;
        if (array_key_exists("page", $_GET)) {
            $page = $_GET['page'];
        }
        $arrMovie = $this->model('MovieModel')->searchMovie($keyword, $page);

        echo json_encode($arrMovie);
    }

    public function chairCheck()
    {
        $data = new \stdClass();
        $data->status = 200;

        $data->data = $this->model('BookModel')->getDisabledSeat();
        $response = json_encode($data);
        echo $response;
    }

    public function deleteReview()
    {
        $auth = new \core\Auth;
        if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
            $auth->checkAuthenticated();
            $data = new \stdClass();
            $data->status = 200;
            if ($this->model('RatingModel')->delete()) {
                $data->result = true;
            } else {
                $data->result = false;
            }
        }
        
        $response = json_encode($data);
        echo $response;
    }

    public function insertSchedule()
    {
        $data = new \stdClass();
        $data->status = 200;
        $data->data = $this->model('ScheduleModel')->insertSchedule();
        $response = json_encode($data);
        echo $response;
    }
}
