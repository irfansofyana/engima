<?php

// URL
define('BASEURL', 'http://localhost/engima/public/');
// define('BASEURL', 'http://ec2-54-227-190-218.compute-1.amazonaws.com/engima/public/');
define('MOVIEIMAGEURL', 'https://image.tmdb.org/t/p/w600_and_h900_bestv2');

// DATABASE
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'tubes1');
