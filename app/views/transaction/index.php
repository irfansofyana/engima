<div class="content">
    <div class="container">
        <h1 class="main-title">Transaction History</h1>
        <?php
            $books = $data["books"];
        for ($i = 0; $i < count($books); $i++) {
            $book = $books[$i];
            ?>
        <div class="transaction-wrapper">
            <div class="row">
                <div class="col-1 transaction-poster-wrapper">
                    <img 
                        src=<?php echo MOVIEIMAGEURL . $book['movie']['poster'] ?> 
                        class="transaction-poster"
                    >
                </div>
                <div class="col-9 transaction-detail px-auto">
                    <div class="transaction-title">
                    <?php echo $book["movie"]["title"] ?>
                    </div>
                    <div class="transaction-schedule">
                        <span>Schedule: </span>
                    <?php
                        echo date_format(
                            date_create($book["dateTime"]),
                            "F d, Y - h:i A"
                        );
                    ?>
                    </div>
                    <?php
                    date_default_timezone_set('Asia/Jakarta');
                    if (strtotime($book["dateTime"]) <= time() && $book["status"] == "SUCCESS") {
                        if ((int) $book["isRate"] == 0) {
                            ?>
                    <div class="btn-wrapper">
                        <a 
                            class="btn btn-primary btn-transaction" 
                            href="<?php echo BASEURL . "rating/new?book-id=" .
                            $book["idTransaksi"] ?>"
                        >
                            Add Review
                        </a>
                    </div>
                            <?php
                        } else {
                            ?>
                    <div class="transaction-text">
                        Your review has been submitted
                    </div>
                    <input 
                        type="hidden" 
                        class="book-id" 
                        value="<?php echo $book["idTransaksi"] ?>"
                    >
                    <input 
                        type="hidden" 
                        class="movie-id" 
                        value="<?php echo $book["idMovie"] ?>"
                    >
                    <div class="btn-wrapper">
                        <button 
                            class="btn btn-danger btn-transaction btn-delete-review" 
                            id="<?php echo $i?>"
                        >
                            Delete Review
                        </button>
                        <a 
                            class="btn btn-success btn-transaction" 
                            href="<?php echo BASEURL . "rating/edit?book-id=" .
                            $book["idTransaksi"] ?>"
                        >
                            Edit Review
                        </a>
                    </div>
                    <?php
                        }
                    } elseif (strtotime($book["dateTime"]) > time() && $book["status"] == "SUCCESS") {
                    ?>
                    <div class="transaction-text">
                        Pembayaran berhasil
                    </div>
                    <?php
                    } elseif ($book["status"] == "PENDING") {
                    ?>
                    <div class="transaction-text">
                        Lakukan transfer ke <?php echo $book["virtualNumber"] ?>
                    </div>
                    <input hidden class="time-input" value=<?php echo 2*60 - (time() - strtotime($book["waktu"]))?> />
                    <div class="transaction-text time-remaining">
                        Lakukan transer sebelum <?php echo 2*60 - (time() - strtotime($book["waktu"]))?> detik
                    </div>
                    <?php
                    } elseif ($book["status"] == "CANCELLED") {
                    ?>
                    <div class="transaction-text">
                        Transaksi gagal
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
            <?php
            if ($i + 1 != count($books)) {
                echo "<hr>";
            }
        }
        ?>
    </div>
</div>