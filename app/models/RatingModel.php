<?php namespace models;

class RatingModel
{
    private $db;
    private $auth;
  
    public function __construct()
    {
        $this->db = new \core\Database;
        $this->auth = new \core\Auth;
        $this->curl = curl_init();
    }

    public function getRatingByIdBook($idBook)
    {
        $query = "SELECT idReview, value, text 
        FROM Review
        WHERE idBook = :idBook";

        $this->db->query($query);
        $this->db->bind('idBook', $idBook);

        $data = $this->db->resultSet();
        return $data[0];
    }

    public function insert()
    {
        $idMovie = $_GET["movie-id"];
        $idUser = $this->auth->getUserId();
        
        $idTransaction = $_GET["book-id"];

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/getTransaction/".$idTransaction,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));
        
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);

        if ($err) {
            return false;
        } else {
            $data = json_decode($response, true);
            if ($data[0]["idUser"] != $idUser && $data[0]["idMovie"] != $idMovie) {
                return false;
            }
        }

        $query = "INSERT INTO Review
        (idMovie, idUser, idBook, value, text)
        VALUES (:idMovie, :idUser, :idBook, :value, :text)";

        $this->db->query($query);
        $this->db->bind('idMovie', $idMovie);
        $this->db->bind('idUser', $idUser);
        $this->db->bind('idBook', $idTransaction);
        $this->db->bind('value', $_POST["value"]);
        $this->db->bind('text', $_POST["review"]);
        $data = $this->db->execute();

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/changeRateTransaction",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"idTransaction\":".$idTransaction.", \"isRate\":1}",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json;charset=utf-8"
            ),
        ));

        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);

        curl_close($this->curl);

        return true;
    }

    public function update()
    {
        $idReview = $_GET["review-id"];

        $query = "UPDATE Review
        SET value = :value, text = :text 
        WHERE idReview = :idReview";

        $this->db->query($query);
        $this->db->bind('value', $_POST["value"]);
        $this->db->bind('text', $_POST["review"]);
        $this->db->bind('idReview', $idReview);
        try {
            $data = $this->db->execute();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function delete()
    {
    
        $idBook = (int) $_GET["book-id"];
        $idUser = (int) $this->auth->getUserId();

        $query = "SELECT idReview
        FROM Review 
        WHERE idUser = :idUser
        AND idBook = :idBook";
        
        $this->db->query($query);
        $this->db->bind('idBook', $idBook);
        $this->db->bind('idUser', $idUser);

        try {
            $data = $this->db->resultSet();
            if (count($data) == 0) {
                return false;
            } else {
                $idReview = $data[0]["idReview"];
            }
        } catch (Exception $e) {
            return false;
        }

        $query = "DELETE FROM Review
            WHERE idReview = :idReview";

        $this->db->query($query);
        $this->db->bind('idReview', $idReview);

        try {
            $this->db->execute();
        } catch (Exception $e) {
            return false;
        }

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/changeRateTransaction",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"idTransaction\":".$idBook.", \"isRate\":0}",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json;charset=utf-8"
            ),
        ));

        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);

        curl_close($this->curl);

        return true;
    }
}
