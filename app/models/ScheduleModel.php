<?php namespace models;

class ScheduleModel
{
    private $db;

    public function __construct()
    {
        $this->db = new \core\Database;
        $this->curl = curl_init();
    }
    public function getScheduleByID()
    {

        $movie_id = $_GET["movie-id"];
        $schedule_id = $_GET["schedule-id"];
        $query = "SELECT * FROM Schedule WHERE Schedule.idSchedule = :schedule_id AND Schedule.idMovie = :movie_id";
        $this->db->query($query);
        $this->db->bind('schedule_id', $schedule_id);
        $this->db->bind('movie_id', $movie_id);

        $data = $this->db->resultSet();
        $idMovie = $data[0]["idMovie"];

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "https://api.themoviedb.org/3/movie/".$idMovie
                . "?api_key=fc04a2bcf984886d928f8aa556dbbbf8&language=en-US",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));

        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        
        curl_close($this->curl);
        if ($err) {
            $data[0]["movie"] = null;
        } else {
            $data_movie = json_decode($response, true);
            $data[0]["movie"] = $data_movie;
        }
        return $data[0];
    }

    public function isScheduleExist()
    {

        $movie_id = $_GET["movie-id"];
        $schedule_id = $_GET["schedule-id"];
        $query = "SELECT * FROM Schedule
        WHERE idSchedule = :schedule_id AND idMovie = :movie_id";

        $this->db->query($query);
        $this->db->bind('schedule_id', $schedule_id);
        $this->db->bind('movie_id', $movie_id);

        $data = $this->db->resultSet();

        return $data;
    }

    public function isScheduleExistByIdMovie($movie_id)
    {
        $query = "SELECT * FROM Schedule WHERE idMovie = :movie_id";
        $this->db->query($query);
        $this->db->bind('movie_id', $movie_id);

        $data = $this->db->resultSet();
        return (count($data) > 0);
    }

    public function insertSchedule()
    {
        $movie_id = $_POST["movieid"];
        $date_time = $_POST["datetime"];
        $idScheduleExist = $this->isScheduleExistByIdMovie($movie_id);
        if (!($idScheduleExist)) {
            $query = "INSERT INTO Schedule(idMovie, dateTime, seatsLeft)
            VALUES(:movie_id, :date_time, 30)";
            $this->db->query($query);
            $this->db->bind('movie_id', $movie_id);
            $this->db->bind('date_time', $date_time);
            try {
                $this->db->execute();
                return true;
            } catch (\Throwable $th) {
                return false;
            }
        }
        return true;
    }
}
