<?php namespace models;

class BookModel
{
    private $db;
    private $auth;

    public function __construct()
    {
        $this->db = new \core\Database;
        $this->auth = new \core\Auth;
        $this->curl = curl_init();
    }

    public function getAllBook()
    {
        $idUser = $this->auth->getUserId();
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/getAllTransaction/".$idUser,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));
        
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        
        curl_close($this->curl);

        if ($err) {
            return array();
        } else {
            $data = json_decode($response, true);
            return $data;
        }
    }

    public function getBookById()
    {
        $idTransaction = $_GET["book-id"];

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/getTransaction/".$idTransaction,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));
        
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        
        curl_close($this->curl);

        if ($err) {
            return array();
        } else {
            $data = json_decode($response, true);
            return $data;
        }
    }

    public function formatBook($data)
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "https://api.themoviedb.org/3/movie/".$data["idMovie"]
                ."?language=en-US&api_key=fc04a2bcf984886d928f8aa556dbbbf8",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));
        
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);

        if ($err) {
            $data["movie"] = null;
        } else {
            $temp = array();
            $res = json_decode($response, true);
            $temp["title"] =  $res["original_title"];

            $data["movie"] = $temp;
        }

        return $data;
    }

    public function checkTransaction($books)
    {
        for ($i=0; $i<count($books); $i++) {
            $book = $books[$i];
            date_default_timezone_set('Europe/London');
            $timeNow = date('Y-m-d H:i:s');
            $timeBefore = date("Y-m-d H:i:s", strtotime("-2 minute"));
            $timeBook = date('Y-m-d H:i:s', strtotime($book["waktu"]));

            if ($book["status"] == "PENDING" && $timeBefore < $timeBook) {
                $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"'
                . ' xmlns:rek="http://rekening.server/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <rek:check>
                         <nomorRekening>'.$book["virtualNumber"].'</nomorRekening>
                         <nominal>45000</nominal>
                         <startdate>'.$timeBefore.'</startdate>
                         <enddate>'.$timeNow.'</enddate>
                      </rek:check>
                   </soapenv:Body>
                </soapenv:Envelope>';

                $headers = array(
                    "Content-type: text/xml");
                
                curl_setopt_array($this->curl, array(
                    CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:8080/web/CheckTransaksi",
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_POST => 1,
                    CURLOPT_HTTPHEADER => $headers,
                    CURLOPT_POSTFIELDS => $request,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_VERBOSE => true,
                    CURLOPT_TIMEOUT =>10
                ));
                
                $response = curl_exec($this->curl);
                $err = curl_error($this->curl);

                if (!$err) {
                    if (strpos($response, 'true')) {
                        curl_setopt_array($this->curl, array(
                            CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/"
                                . "changeStatusTransaction",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "{\"idTransaction\":".$book["idTransaksi"].",\"status\":\"SUCCESS\"}",
                            CURLOPT_HTTPHEADER => array(
                                "content-type: application/json;charset=utf-8"
                            ),
                        ));

                        $response = curl_exec($this->curl);
                        $err = curl_error($this->curl);
                    }
                }
            } elseif ($book["status"] == "PENDING" && $timeBefore > $timeBook) {
                curl_setopt_array($this->curl, array(
                    CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/changeStatusTransaction",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "{\"idTransaction\":".$book["idTransaksi"].",\"status\":\"CANCELLED\"}",
                    CURLOPT_HTTPHEADER => array(
                        "content-type: application/json;charset=utf-8"
                    ),
                ));

                $response = curl_exec($this->curl);
                $err = curl_error($this->curl);

                $query = "UPDATE Schedule SET seatsLeft = seatsLeft+1 WHERE idSchedule = :schedule_id";
                $this->db->query($query);
                $this->db->bind('schedule_id', $book["idSchedule"]);
                $this->db->execute();
            }
        }
    }

    public function formatAllBook($data)
    {
        for ($i=0; $i<count($data); $i++) {
            $movie = $data[$i];

            curl_setopt_array($this->curl, array(
                CURLOPT_URL => "https://api.themoviedb.org/3/movie/".$movie["idMovie"]
                    ."?language=en-US&api_key=fc04a2bcf984886d928f8aa556dbbbf8",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "{}",
            ));
            
            $response = curl_exec($this->curl);
            $err = curl_error($this->curl);

            if ($err) {
                $data[$i]["movie"] = null;
            } else {
                $query = "SELECT * FROM Schedule 
                WHERE idSchedule = :schedule_id";
                $this->db->query($query);
                $this->db->bind('schedule_id', $data[$i]["idSchedule"]);
                $data_schedule = $this->db->resultSet();

                $temp = array();
                $res = json_decode($response, true);
                $temp["title"] =  $res["original_title"];
                $temp["poster"] = $res["poster_path"];

                $data[$i]["movie"] = $temp;
                $data[$i]["dateTime"] = $data_schedule[0]["dateTime"];
            }
        }
        return $data;
    }

    public function getDisabledSeat()
    {

        $schedule_id = $_GET['schedule-id'];

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/getDisabledSeat/".$schedule_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));

        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        
        curl_close($this->curl);

        if ($err) {
            return array();
        } else {
            $data = json_decode($response, true);
            return $data;
        }
    }

    public function insertSeat($data, $idUser)
    {
        $virtual = $data['virtual-number'];
        $seat = $data['seat-number'];
        $movie_id = $data['movie-id'];
        $schedule_id = $data['schedule-id'];

        curl_setopt_array($this->curl, array(

            CURLOPT_URL => "http://ec2-18-212-188-140.compute-1.amazonaws.com:3005/createNewTransaction",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"idUser\":".$idUser
                . ",\"idMovie\":".$movie_id
                . ",\"idSchedule\":".$schedule_id
                . ",\"chair\":".$seat
                . ",\"virtualNumber\":".$virtual."}",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json;charset=utf-8"
            ),
        ));

        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        
        curl_close($this->curl);

        if ($err) {
            return -1;
        } else {
            $this->reduceSeatLeft($schedule_id);
            return $response;
        }
    }

    private function reduceSeatLeft($schedule_id)
    {
        $query = "UPDATE Schedule 
        SET seatsLeft = seatsLeft - 1 
        WHERE idSchedule = :schedule_id";

        $this->db->query($query);
        $this->db->bind('schedule_id', $schedule_id);

        try {
            $this->db->execute();
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }
}
