<?php namespace models;

class MovieModel
{
    private $db;

    public function __construct()
    {
        $this->db = new \core\Database;
        $this->curl = curl_init();
    }

    public function getPlayingMovie()
    {
        date_default_timezone_set('Asia/Jakarta');
        $timeNow = date('Y-m-d');
        $timeLater = date("Y-m-d", strtotime("-7 day"));

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "https://api.themoviedb.org/3/discover/movie?"
                . "api_key=fc04a2bcf984886d928f8aa556dbbbf8&language=en-US&"
                . "region=US&release_date.gte=".$timeLater."&&release_date.lte=".$timeNow
                . "&with_release_type=2|3",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));
        
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        
        curl_close($this->curl);
        
        if ($err) {
            return array();
        } else {
            $data = json_decode($response, true);
            return $data["results"];
        }
    }

    public function getSingleMovie($idMovie)
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "https://api.themoviedb.org/3/movie/"
                .$idMovie."?api_key=fc04a2bcf984886d928f8aa556dbbbf8&language=en-US",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));
        
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        
        curl_close($this->curl);
        
        if ($err) {
            return array();
        } else {
            $data = json_decode($response, true);
            return $data;
        }
    }

    public function getMovieSchedule($idMovie)
    {
        $query = "SELECT * 
        FROM Schedule 
        WHERE idMovie = :id
        ORDER BY dateTime";
        $this->db->query($query);
        $this->db->bind("id", $idMovie);
        $data = $this->db->resultSet();

        return $data;
    }

    public function getMovieReview($idMovie)
    {
        $queryReview = "SELECT * 
        FROM Review NATURAL JOIN User
        WHERE idMovie = :id";
        $this->db->query($queryReview);
        $this->db->bind("id", $idMovie);
        $data = $this->db->resultSet();

        return $data;
    }

    public function getRating($idMovie)
    {

        $queryRating = "SELECT avg(value) AS 'rating'
        FROM Review
        WHERE idMovie = :idMovie";

        $this->db->query($queryRating);
        $this->db->bind('idMovie', $idMovie);
        $data = $this->db->resultSet();
        $rating = $data[0]["rating"];

        try {
            $this->db->execute();
        } catch (Exception $e) {
            return 0;
        }
        return $rating;
    }

    public function updateRating()
    {
        $idMovie = $_GET["movie-id"];

        $queryRating = "SELECT avg(value) AS 'rating'
        FROM Review
        WHERE idMovie = :idMovie";

        $this->db->query($queryRating);
        $this->db->bind('idMovie', $idMovie);
        $data = $this->db->resultSet();
        $rating = $data[0]["rating"];

        $queryUpdate = "UPDATE Movie
        SET rating = :rating
        WHERE idMovie = :idMovie";
        
        $this->db->query($queryUpdate);
        $this->db->bind('rating', $rating);
        $this->db->bind('idMovie', $idMovie);
        
        try {
            $this->db->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function searchMovie($keyword, $page)
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "https://api.themoviedb.org/3/search/movie?"
                ."api_key=fc04a2bcf984886d928f8aa556dbbbf8&language=en-US&"
                . "page=".$page."&query=".$keyword,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
        ));
        
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        
        curl_close($this->curl);
        
        if ($err) {
            return array();
        } else {
            $data = json_decode($response, true);
            return $data;
        }
    }
}
