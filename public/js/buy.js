var movie_title = document.getElementById("movie-title-id");
var movie_time = document.getElementById("movie-time-id");
var chairs = document.getElementsByClassName("chair");
var summary_wrapper = document.getElementById("summary-wrapper-id");
var input_seat_number = document.getElementById("input-seat-number");
var input_schedule_id = document.getElementById("input-schedule-id");
var input_movie_id = document.getElementById("input-movie-id");
var back_btn = document.getElementById("back-btn-id");
var buy_modal_wrapper = document.getElementById("buy-modal-wrapper-id");
var content = document.getElementsByClassName("content");
var buy_container = content[0].getElementsByClassName("container");
var chair_layout = document.getElementsByClassName("chair");
var time_out = 3000;


function getVirtualNumber(callback) {
    var xhr = new XMLHttpRequest();
    var virtualRekening;

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(xhr.responseText,"text/xml");
                virtualRekening = xmlDoc.getElementsByTagName("return")[0].childNodes[0].nodeValue;
                
                callback(virtualRekening);
            }
        }
    }
    
    var sr = `
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rek="http://rekening.server/">
           <soapenv:Header/>
           <soapenv:Body>
              <rek:makeVirtualRekening>
                 <nomor_rekening>123400000002</nomor_rekening>
              </rek:makeVirtualRekening>
           </soapenv:Body>
        </soapenv:Envelope>
    `;

    xhr.open('POST', 'http://ec2-18-212-188-140.compute-1.amazonaws.com:8080/web/MakeVirtualRekening', true);
    xhr.setRequestHeader("Content-Type", "'text/xml'");
    xhr.send(sr);
}

function wrapper(number) {
    var title = movie_title.innerText;
    var time = movie_time.innerText;
    return `
        <div class="selected">
            <div class="movie-summary">
                <div class="summary-title">
                    ${title}
                </div>
                <div class="summary-subtitle">
                    ${time}
                </div>
            </div>
            <div class="seat-summary">
                <div class="seat">
                    Seat #${number}
                </div>
            <div class="price">
                Rp 45.000
            </div>
        </div>
    <button class="buy-ticket" id="buy-ticket-id">
        Buy Ticket
    </button>
    </div>`;
}


for (var i = 0; i < chairs.length; i++) {
    chairs[i].addEventListener("click", function () {
        var temp = this.textContent
        input_seat_number.value = temp;
        summary_wrapper.innerHTML = wrapper(temp);

        var buy_btn = document.getElementById("buy-ticket-id");
        buy_btn.addEventListener("click", async function () {
            await getVirtualNumber(function(number) {
                if (number != "987600000000") {
                    var xhr = new XMLHttpRequest();

                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4) {
                            if (xhr.status == 200) {
                                buy_container[0].style.opacity = 0.5;
                                buy_modal_wrapper.style.display = "flex";
                                var temp = JSON.parse(xhr.responseText);
                                if (!temp.result) {
                                    var title = document.getElementById("payment-title-id");
                                    var desc = document.getElementById("payment-desc-id");
                                    var btn = document.getElementById("goto-transaction-id");
                                    title.innerText = "Payment Fail!";
                                    desc.innerText = "Please check your booking seat again."
                                    btn.style.display = "none";
                                } else {
                                    var desc = document.getElementById("payment-desc-id");
                                    desc.innerText = `Id Transaksi kamu ${temp.idTransaksi}\nSilahkan bayar ke ${number} dengan nominal Rp 45.000`;
                                }
                            }
                        }
                    }

                    var param = `schedule-id=${input_schedule_id.value}&movie-id=${input_movie_id.value}&seat-number=${temp}&virtual-number=${number}`;
                    xhr.open('POST', 'http://ec2-54-227-190-218.compute-1.amazonaws.com/engima/public//api/book', true);
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.send(param);
                } else {
                    buy_container[0].style.opacity = 0.5;
                    buy_modal_wrapper.style.display = "flex";
                    var title = document.getElementById("payment-title-id");
                    var desc = document.getElementById("payment-desc-id");
                    var btn = document.getElementById("goto-transaction-id");
                    title.innerText = "Payment Fail!";
                    desc.innerText = "Terjadi Kesalahan"
                    btn.style.display = "none";
                }
            });
        })
    })
}

back_btn.addEventListener("click", function () {
    window.location.replace(`detail?id=${input_movie_id.value}`);
})

window.onclick = function (event) {
    if (event.target == buy_modal_wrapper) {
        window.location.reload();
    }
}

function fetchchair() {
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var response = JSON.parse(xhr.responseText);
                response.data.forEach(function (item) {
                    chair_layout[item.chair - 1].disabled = true;
                });
                setTimeout(fetchchair, time_out);
            }
        }
    }
    var param = `schedule-id=${input_schedule_id.value}`;
    var url = 'http://ec2-54-227-190-218.compute-1.amazonaws.com/engima/public/api/chairCheck?' + param
    xhr.open('GET', url, true);
    xhr.send();
}

window.onload = function () {
    this.setTimeout(fetchchair, time_out);
};

