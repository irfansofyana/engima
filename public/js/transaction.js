var btnDelete = document.getElementsByClassName('btn-delete-review');
var bookId = document.getElementsByClassName('book-id');
var movieId = document.getElementsByClassName('movie-id');
var timesInput = document.getElementsByClassName('time-input');
var timeRemaining = document.getElementsByClassName('time-remaining');
var arrBtn = [];

for (let i = 0; i < btnDelete.length; i++) {
    arrBtn.push(btnDelete[i]);
}

for (let i = 0; i < btnDelete.length; i++) {
    btnDelete[i].addEventListener('click', function () {
        var id = arrBtn.indexOf(this);
        var idBook = bookId[id].value;
        var idMovie = movieId[id].value;

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                location.reload();
            }
        }
        xhr.open('DELETE', 'api/deleteReview?book-id=' + idBook + '&movie-id=' + idMovie);
        xhr.send();
    })
}

function remain() {
    for (let i=0; i< timesInput.length; i++) {
        timesInput[i].value = timesInput[i].value - 1;
        if (timesInput[i].value > 0) {
            timeRemaining[i].innerHTML = `Lakukan transer sebelum ${timesInput[i].value} detik`
        } else {
            timeRemaining[i].innerHTML = `Waktu habis`
        }
    }
}

window.onload = function () {
    this.setInterval(remain, 1000);
};